﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace SmartCalendarWP.View
{
    public partial class CalendarWeekControl : UserControl
    {
        public delegate void NavigationHandler(object source, NavigationEventArgs e);
        private event NavigationHandler navigateParentReady;
        
        public event NavigationHandler NavigateParentReady 
        {
            add {
                this.navigateParentReady += value;
            }
            remove
            {
                this.navigateParentReady -= value;
            }
        }

        protected void OnNavigateParentReady(NavigationEventArgs e)
        {
            if (navigateParentReady != null)
            {
                navigateParentReady(this, e);
            }
        }

        public CalendarWeekControl()
        {
            InitializeComponent();
        }

        private void Day_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var ev = new NavigationEventArgs(null, new Uri("/DayPage.xaml", UriKind.Relative));

            OnNavigateParentReady(ev);
        }
    }
}
