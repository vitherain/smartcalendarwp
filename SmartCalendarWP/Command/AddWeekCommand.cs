﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SmartCalendarWP.Command
{
    public class AddWeekCommandParameter
    {
        public AddingActionType ActionType
        {
            get;
            private set;
        }

        public int AffectedWeekNumber
        {
            get;
            private set;
        }

        public AddWeekCommandParameter(AddingActionType actionType, int affectedWeekNumber)
        {
            this.ActionType = actionType;
            this.AffectedWeekNumber = affectedWeekNumber;
        }
    }

    public enum AddingActionType
    {
        Newer = 0,
        Elder = 1
    }

    public class AddWeekCommand : ICommand
    {
        private Predicate<object> canExecute;
        private Action<object> execute;
        private event EventHandler CanExecuteChangedInternal;

        public AddWeekCommand(Action<object> execute)
             : this(o => true, execute)
        {
        }

        public AddWeekCommand(Predicate<object> canExecute, Action<object> execute)
        {
            this.canExecute = canExecute;
            this.execute = execute;
        }

        public bool CanExecute(object parameter)
        {
            return this.canExecute != null && this.canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            this.execute(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                this.CanExecuteChangedInternal += value;
            }

            remove
            {
                this.CanExecuteChangedInternal -= value;
            }
        }

        public void OnCanExecuteChanged()
        {
            if (this.CanExecuteChangedInternal != null)
            {
                this.CanExecuteChangedInternal(this, EventArgs.Empty);
            }
        }
    }
}
