﻿using SmartCalendarWP.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCalendarWP
{
    public static class Utils
    {
        public static WeekModel FindEarliestWeek(Collection<WeekModel> weekList)
        {
            WeekModel earliest = weekList[0];

            foreach (WeekModel week in weekList)
            {

                if (week.Day1.Date < earliest.Day1.Date)
                {
                    earliest = week;
                }
            }

            return earliest;
        }

        public static WeekModel FindLatestWeek(Collection<WeekModel> weekList)
        {
            WeekModel latest = weekList[0];

            foreach (WeekModel week in weekList)
            {

                if (week.Day1.Date > latest.Day1.Date)
                {
                    latest = week;
                }
            }

            return latest;
        }
    }
}
