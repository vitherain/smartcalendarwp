﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCalendarWP.Model
{
    public class CalendarDay
    {
        public DateTime Date { get; private set; }
        public string Label { get; set; }

        public CalendarDay(DateTime date)
            : this(date, string.Empty)
        {
        }

        public CalendarDay(DateTime date, string label)
        {
            this.Date = date;
            this.Label = label;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
