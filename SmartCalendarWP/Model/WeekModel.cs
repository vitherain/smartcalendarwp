﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartCalendarWP.Model
{
    public class WeekModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsCurent
        {
            get;
            set;
        }

        private string header;

        public string Header
        {
            get
            {
                return this.header;
            }
            set
            {
                if (Equals(this.header, value)) return;

                header = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Header"));
            }
        }

        private CalendarDay day1;

        public CalendarDay Day1
        {
            get
            {
                return this.day1;
            }
            set
            {
                if (Equals(this.day1, value)) return;

                day1 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Day1"));
            }
        }

        private CalendarDay day2;

        public CalendarDay Day2
        {
            get
            {
                return this.day2;
            }
            set
            {
                if (Equals(this.day2, value)) return;

                day2 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Day2"));
            }
        }

        private CalendarDay day3;

        public CalendarDay Day3
        {
            get
            {
                return this.day3;
            }
            set
            {
                if (Equals(this.day3, value)) return;

                day3 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Day3"));
            }
        }

        private CalendarDay day4;

        public CalendarDay Day4
        {
            get
            {
                return this.day4;
            }
            set
            {
                if (Equals(this.day4, value)) return;

                day4 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Day4"));
            }
        }

        private CalendarDay day5;

        public CalendarDay Day5
        {
            get
            {
                return this.day5;
            }
            set
            {
                if (Equals(this.day5, value)) return;

                day5 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Day5"));
            }
        }

        private CalendarDay day6;

        public CalendarDay Day6
        {
            get
            {
                return this.day6;
            }
            set
            {
                if (Equals(this.day6, value)) return;

                day6 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Day6"));
            }
        }

        private CalendarDay day7;

        public CalendarDay Day7
        {
            get
            {
                return this.day7;
            }
            set
            {
                if (Equals(this.day7, value)) return;

                day7 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Day7"));
            }
        }

        private string month;

        public string Month
        {
            get
            {
                return this.month;
            }
            set
            {
                if (Equals(this.month, value)) return;

                month = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Month"));
            }
        }

        protected void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, args);
            }
        }
        
    }
}
