﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using SmartCalendarWP.Resources;
using SmartCalendarWP.ViewModel;
using SmartCalendarWP.Model;
using System.Collections.ObjectModel;
using SmartCalendarWP.Command;
using SmartCalendarWP.View;

namespace SmartCalendarWP
{
    public partial class MainPage : PhoneApplicationPage
    {
        private int lastIndex = -1;

        public MainPage()
        {
            InitializeComponent();

            ((CalendarWeekControl)this.Week0).NavigateParentReady += MainPage_NavigateParentReady;
            ((CalendarWeekControl)this.Week1).NavigateParentReady += MainPage_NavigateParentReady;
            ((CalendarWeekControl)this.Week2).NavigateParentReady += MainPage_NavigateParentReady;
            ((CalendarWeekControl)this.Week3).NavigateParentReady += MainPage_NavigateParentReady;

            BuildLocalizedApplicationBar();
        }

        private void BuildLocalizedApplicationBar()
        {
            ApplicationBar = new ApplicationBar();

            ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/add.png", UriKind.Relative));
            appBarButton.Text = AppResources.AddTask;
            ApplicationBar.Buttons.Add(appBarButton);

            ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
            ApplicationBar.MenuItems.Add(appBarMenuItem);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.Pivot.SelectedIndex = 1;
        }

        protected void Pivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int affectedWeekNumber = -1;
            AddWeekCommandParameter parameter;

            if (lastIndex != -1)
            {
                if (this.Pivot.SelectedIndex - 1 == lastIndex || this.Pivot.SelectedIndex - lastIndex == -3)
                {
                    affectedWeekNumber = this.Pivot.SelectedIndex + 2;

                    if (affectedWeekNumber > 3)
                    {
                        affectedWeekNumber -= 4;
                    }

                    parameter = new AddWeekCommandParameter(AddingActionType.Newer, affectedWeekNumber);
                }
                else
                {
                    affectedWeekNumber = this.Pivot.SelectedIndex - 1;

                    if (affectedWeekNumber < 0)
                    {
                        affectedWeekNumber += 4;
                    }

                    parameter = new AddWeekCommandParameter(AddingActionType.Elder, affectedWeekNumber);
                }

                ((WeekViewModel)this.DataContext).AddWeekCommand.Execute(parameter);
            }

            lastIndex = this.Pivot.SelectedIndex;
        }

        private void MainPage_NavigateParentReady(object source, NavigationEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Jsem v navigated a navigovat mam na " + e.Uri);
            ((Frame)Parent).Navigate(e.Uri);
        }
    }
}