﻿using SmartCalendarWP.Command;
using SmartCalendarWP.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SmartCalendarWP.ViewModel
{
    public class WeekViewModel : INotifyPropertyChanged
    {
        private DateTimeFormatInfo dateTimeFormatInfo = new CultureInfo("cs-CZ").DateTimeFormat;
        //private DateTimeFormatInfo dateTimeFormatInfo = DateTimeFormatInfo.CurrentInfo;

        private WeekModel week0;
        public WeekModel Week0
        {
            get
            {
                return week0;
            }
            set
            {
                if (Equals(this.week0, value)) return;

                week0 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Week0"));
            }
        }
        
        private WeekModel week1;
        public WeekModel Week1
        {
            get
            {
                return week1;
            }
            set
            {
                if (Equals(this.week1, value)) return;

                week1 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Week1"));
            }
        }

        private WeekModel week2;
        public WeekModel Week2
        {
            get
            {
                return week2;
            }
            set
            {
                if (Equals(this.week2, value)) return;

                week2 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Week2"));
            }
        }

        private WeekModel week3;
        public WeekModel Week3
        {
            get
            {
                return week3;
            }
            set
            {
                if (Equals(this.week3, value)) return;

                week3 = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Week3"));
            }
        }

        public ICommand AddWeekCommand
        {
            get;
            private set;
        }
            

        public event PropertyChangedEventHandler PropertyChanged;

        public WeekViewModel()
        {
            AddWeekCommand = new AddWeekCommand(obj => 
            {
                IList<WeekModel> list = MakeListOfWeeks();

                AddWeekCommandParameter parameter = obj as AddWeekCommandParameter;

                if (parameter != null)
                {
                    WeekModel weekToInsert = null;

                    int referenceWeekIndex = -1;
                    WeekModel referenceWeek;

                    if (parameter.ActionType == AddingActionType.Newer)
                    {
                        referenceWeekIndex = parameter.AffectedWeekNumber - 1;

                        if (referenceWeekIndex < 0)
                        {
                            referenceWeekIndex += 4;
                        }

                        referenceWeek = list[referenceWeekIndex];

                        weekToInsert = CreateWeekModel(referenceWeek.Day1.Date.AddDays(7), false, dateTimeFormatInfo);
                    }
                    else if (parameter.ActionType == AddingActionType.Elder)
                    {
                        referenceWeekIndex = parameter.AffectedWeekNumber + 1;

                        if (referenceWeekIndex > 3)
                        {
                            referenceWeekIndex -= 4;
                        }

                        referenceWeek = list[referenceWeekIndex];

                        weekToInsert = CreateWeekModel(referenceWeek.Day1.Date.AddDays(-7), false, dateTimeFormatInfo);
                    }

                    list.RemoveAt(parameter.AffectedWeekNumber);
                    list.Insert(parameter.AffectedWeekNumber, weekToInsert);

                    SetWeeksFromList(list);
                }
            });

            InitializeWeeks();
        }

        private IList<WeekModel> MakeListOfWeeks()
        {
            IList<WeekModel> list = new List<WeekModel>();
            list.Add(this.Week0);
            list.Add(this.Week1);
            list.Add(this.Week2);
            list.Add(this.Week3);

            return list;
        }

        private void SetWeeksFromList(IList<WeekModel> list)
        {
            this.Week0 = list[0];
            this.Week1 = list[1];
            this.Week2 = list[2];
            this.Week3 = list[3];
        }

        private void InitializeWeeks()
        {
            DateTime today = DateTime.Today;
            DateTime firstDayInCurrentWeek = GetFirstDayInWeek(today, dateTimeFormatInfo);

            Week0 = CreateWeekModel(firstDayInCurrentWeek.AddDays(-7), false, dateTimeFormatInfo);
            Week1 = CreateWeekModel(firstDayInCurrentWeek, true, dateTimeFormatInfo);
            Week2 = CreateWeekModel(firstDayInCurrentWeek.AddDays(7), false, dateTimeFormatInfo);
            Week3 = CreateWeekModel(firstDayInCurrentWeek.AddDays(14), false, dateTimeFormatInfo);

        }

        private DateTime GetFirstDayInWeek(DateTime currentDay, DateTimeFormatInfo dateTimeFormatInfo)
        {
            int currentDaysWeek = dateTimeFormatInfo.Calendar.GetWeekOfYear(currentDay, dateTimeFormatInfo.CalendarWeekRule, dateTimeFormatInfo.FirstDayOfWeek);

            for (int i = -1; i >= -7; i--)
            {
                DateTime testDay = currentDay.AddDays(i);
                int testDaysWeek = dateTimeFormatInfo.Calendar.GetWeekOfYear(testDay, dateTimeFormatInfo.CalendarWeekRule, dateTimeFormatInfo.FirstDayOfWeek);

                if (testDaysWeek != currentDaysWeek)
                {
                    return testDay.AddDays(1);
                }
            }

            return DateTime.MinValue;
        }

        private WeekModel CreateWeekModel(DateTime firstDayInWeek, bool isCurrent, DateTimeFormatInfo dateTimeFormatInfo)
        {
            DateTime lastDayOfWeek = firstDayInWeek.AddDays(6);

            string monthDescription = GetMonthDescription(firstDayInWeek, dateTimeFormatInfo, lastDayOfWeek);

            return new WeekModel() { 
                IsCurent = isCurrent,
                Header = String.Format("{0}.{1}. - {2}.{3}.", firstDayInWeek.Day, firstDayInWeek.Month, lastDayOfWeek.Day, lastDayOfWeek.Month),
                Month = monthDescription,
                Day1 = new CalendarDay(firstDayInWeek, String.Format("{0} {1}", dateTimeFormatInfo.DayNames[(int)firstDayInWeek.DayOfWeek].Substring(0, 2), firstDayInWeek.Day)),
                Day2 = new CalendarDay(firstDayInWeek.AddDays(1), String.Format("{0} {1}", dateTimeFormatInfo.DayNames[(int)firstDayInWeek.AddDays(1).DayOfWeek].Substring(0, 2), (firstDayInWeek.AddDays(1).Day))),
                Day3 = new CalendarDay(firstDayInWeek.AddDays(2), String.Format("{0} {1}", dateTimeFormatInfo.DayNames[(int)firstDayInWeek.AddDays(2).DayOfWeek].Substring(0, 2), (firstDayInWeek.AddDays(2).Day))),
                Day4 = new CalendarDay(firstDayInWeek.AddDays(3), String.Format("{0} {1}", dateTimeFormatInfo.DayNames[(int)firstDayInWeek.AddDays(3).DayOfWeek].Substring(0, 2), (firstDayInWeek.AddDays(3).Day))),
                Day5 = new CalendarDay(firstDayInWeek.AddDays(4), String.Format("{0} {1}", dateTimeFormatInfo.DayNames[(int)firstDayInWeek.AddDays(4).DayOfWeek].Substring(0, 2), (firstDayInWeek.AddDays(4).Day))),
                Day6 = new CalendarDay(firstDayInWeek.AddDays(5), String.Format("{0} {1}", dateTimeFormatInfo.DayNames[(int)firstDayInWeek.AddDays(5).DayOfWeek].Substring(0, 2), (firstDayInWeek.AddDays(5).Day))),
                Day7 = new CalendarDay(firstDayInWeek.AddDays(6), String.Format("{0} {1}", dateTimeFormatInfo.DayNames[(int)firstDayInWeek.AddDays(6).DayOfWeek].Substring(0, 2), (firstDayInWeek.AddDays(6).Day)))
            };
        }

        private static string GetMonthDescription(DateTime firstDayInWeek, DateTimeFormatInfo dateTimeFormatInfo, DateTime lastDayOfWeek)
        {
            string month;

            if (firstDayInWeek.Month == lastDayOfWeek.Month)
            {
                month = String.Format("{0} {1}", dateTimeFormatInfo.GetMonthName(firstDayInWeek.Month), firstDayInWeek.Year);
            }
            else
            {
                if (firstDayInWeek.Year != lastDayOfWeek.Year)
                {
                    month = String.Format(
                        "{0} {1} / {2} {3}",
                        dateTimeFormatInfo.GetMonthName(firstDayInWeek.Month),
                        firstDayInWeek.Year,
                        dateTimeFormatInfo.GetMonthName(lastDayOfWeek.Month),
                        lastDayOfWeek.Year);
                }
                else
                {
                    month = String.Format(
                        "{0} / {1} {2}",
                        dateTimeFormatInfo.GetMonthName(firstDayInWeek.Month),
                        dateTimeFormatInfo.GetMonthName(lastDayOfWeek.Month),
                        lastDayOfWeek.Year);
                }
            }
            return month;
        }

        protected void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, args);
            }
        }
    }
}
